# Helicopter Prison Breaks

## What is this?
This short, guided project is the first one, that I have done as part of my [DataQuest](dataquest.io) "Data Scientist in Python" learning path.

It is about prison escapes where a helicopter has been used to assist the prisoner with escaping (or attempting to escape). We will investigate two things:
 - In which single years have the most attempts occured (and how many were there)?
 - In which country/countries have the most attempts occured?

## Why I do/did this
I have already done a much larger and more complex project in Python in late 2021, than the current one, so why this now in early 2024?
I have taken up Python and Data Science again with this DataQuest learning path in order to
 - get back to it all again
 - ensuring that I get best practices under my skin properly and to
 - develop my skills further in the area of Data Science

The purpose of the Helicopter Prison Breaks project was helpning me training basic data analysis skills in Python and Jupyter Notebook.

## Viewing the project
If you want to only read the project, I recommend that you DOWNLOAD the html file "Helicopter-prison-breaks.html" (click the file in the repository on GitLab and then click the download button) AND THEN open it from your local machine - works well with windows and Chrome anyway. Alternatively you can just simply click on the file "Helicopter-prison-breaks.ipynb" which will also render the entire project in your browser, however less pretty.

## Running/editing the project

If you want to be able to run and/or edit the project for some reason, you can clone this repository or just download the files
 - Helicopter-prison-breaks.ipynb
 - helper.py
to same folder/directory on your machine. Then open Jupyter Notebook (See dependencies below) and open the ipynb file. Once opened, you can work with it.


### Dependencies for running/editing
Requires an installation of **Jupyter Notebook** which is open source and can be obtained by either
 - going to [https://www.anaconda.com/download](https://www.anaconda.com/download) and install the Anaconda distribution which comes with Jupyter Notebook and other things relevant to Data Scientists
 - or if you already have Python on your machine and don't want Anaconda, you can "pip install" it which is somewhat more technical, if you are not familiar with that (I am not yet myself, so google it or something ;)

 If you want to work with it outside Jupyter, note the extra dependencies (packages), that the helper is using.

## Acknowledgments
The contents of the helper module are made by DataQuest and copied from dataquest.io.
The project is a guided project on the "Data Scientist in Python" learning path on dataquest.io.

## Project status
Done ^_^
